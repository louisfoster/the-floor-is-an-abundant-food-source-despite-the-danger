# the floor is an abundant food source despite the danger

## Themes

-   Underwater exploration
-   the floor is... (my suggestion :D)
-   futuristic animal

(the trick is to combine all 3.)

So, what is a futuristic animal? Well, futuristic is having very modern tech or design. An animal is a living organism that feeds on organic matter. So I research the simplest animal - a sponge! It turns out sponges are [used by dolphins as a tool](https://en.wikipedia.org/wiki/Sponge#By_dolphins) in an isolated area of Shark Bay, Australia (I'm from Australia!). Isolation and the fact that it's small families, predominantly the females, means that it's a very modern tool for dolphins. Ha! The dolphin is a futuristic animal. They use the sponge to forage on the seafloor while protecting their rostrum aka beak. Ha! Underwater exploration! The floor is an abundant food source despite the danger!

Source materials:

-   [Video about research](https://www.youtube.com/watch?v=-zdzROgOELM)
-   [Video of fish being foraged, the Parapercis Clathrata](https://www.pond5.com/stock-footage/38433045/latticed-sandperch-parapercis-clathrata-up5019.html)
-   [A paper](https://journals.plos.org/plosone/article?id=10.1371/journal.pone.0003868)
-   [Another paper](https://www.researchgate.net/publication/227728677_Sponge_Carrying_by_Dolphins_Delphinidae_Tursiops_sp_A_Foraging_Specialization_Involving_Tool_Use)

## Mechanics

-   Health bar for unprotected foraging damage
-   Foraging with sponge wears out sponge
-   Touching a sponge attaches it to your beak
-   Health bar reduces slowly over time (energy)
-   Health bar reduces as you swim, forage, pursue
-   Health bar increases with each fish you catch
-   Fish appear when you forage near them,
-   Foraged fish will swim for a few moments (away from you) then start burrowing
-   If you forage above the location of fish, you catch it

## Design

-   Level is divided into 15 columns
-   A fish is placed randomly into 1 of the 15 columns
-   The player (dolphin) is either facing towards 0 or 1
-   If the player forrages facing left, then the dolphin will examine the floor aligned with the 0 side of it's bounds, 1 for right
-   If the dolphin forages withing 2 spaces of the fish, the fish will move up to 3 spaces further away from the dolphin (depending on distance from the edge of the level)
-   If the dolphin forages on the location of the fish, it gets more health
-   A health bar indicates how much health remains, which depletes over time and moreso with movement and foraging (esp without a sponge)
-   If the dolphin/player hits a sponge, it will have the sponge on it's beak and wont extra health when foraging
