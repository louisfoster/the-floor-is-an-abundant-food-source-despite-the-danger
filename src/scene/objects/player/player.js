import {mat3, mat4, quat, vec3} from 'gl-matrix';
import {posix} from 'path';

import {SpatialComponent} from '../../../components/spatialComponent';
import {setupShaders} from '../../shaders/shaders';

/**
 * - spatial, located in 3D space
 * - physical, reacts to environment (base == height map point lerp) and
 * other things
 * - not visible
 * - has first person camera (player moves and rotates around up axis (yaw),
 * camera rotates along right axis (pitch))
 */

export default class Player {
  constructor(_delegate) {
    /**
     * Delegate handles:
     *  - getInputtableEntities()
     */
    this.delegate = _delegate;
    this.id = 'player';

    this.isSpatial = true;
    this.spatial = new SpatialComponent(this);
    this.offsetModelMatrix = this.offsetModelMatrix.bind(this);
    this.spatial.position.set(0, 0.5, 0);

    this.getInputtableEntities = this.getInputtableEntities.bind(this);

    this.isUpdatable = true;
    this.update = this.update.bind(this);

    this.isVisible = true;
    this.draw = this.draw.bind(this);

    this.hasSponge = true;
    this.facingRight = true;
    this.previousTime = 0;
    this.forrageTimer = 0;
    this.isForraging = false;

    let gl = this.delegate.glContext();
    this.program;
    this.vao;
    this.vbo;
    this.setShaderData = this.setShaderData.bind(this);
    let floorHeight = 1 / 8;
    let meshArray = {
      // Top
      // top left
      'top-top-left': [0, 1, 1, 0, 0],
      'top-bottom-left': [
        // bottom left
        0,
        0,
        1,
        0,
        0,
      ],
      'top-top-right': [
        // top right
        1,
        1,
        1,
        0,
        0,
      ],
      'top-bottom-right': [
        // bottom right
        1,
        0,
        1,
        0,
        0,
      ],
    };
    let arr = [];
    arr.push(
        ...meshArray['top-top-right'], ...meshArray['top-top-left'],
        ...meshArray['top-bottom-left'],

        ...meshArray['top-bottom-left'], ...meshArray['top-bottom-right'],
        ...meshArray['top-top-right']);

    this.mesh = new Float32Array(arr);
    setupShaders(gl, 'dolphin', 'dolphin', this, () => {
      this.setShaderData(gl);
    });
  }

  setShaderData(gl) {
    this.vao = gl.createVertexArray();
    gl.bindVertexArray(this.vao);

    this.vbo = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, this.vbo);
    gl.bufferData(gl.ARRAY_BUFFER, this.mesh, gl.STATIC_DRAW);

    const vertexAttributeLocation =
        gl.getAttribLocation(this.program, 'a_vertex');
    gl.enableVertexAttribArray(vertexAttributeLocation);
    gl.vertexAttribPointer(
        vertexAttributeLocation, 2, gl.FLOAT, false, 5 * 4, 0);

    const colorAttributeLocation =
        gl.getAttribLocation(this.program, 'a_color');
    gl.enableVertexAttribArray(colorAttributeLocation);
    gl.vertexAttribPointer(
        colorAttributeLocation, 3, gl.FLOAT, false, 5 * 4, 2 * 4);

    this.delegate.objectReady();
  }

  update(time) {
    if (this.isForraging) {
      if (time - this.forrageTimer > 3) {
        this.isForraging = false;
        this.forrageTimer = 0;
      } else {
        this.previousTime = time;
        return;
      }
    }
    if (!this.hasSponge) {
      let pos = this.delegate.spongePosition();

      if (pos.x <= this.spatial.position.x &&
          pos.x >= this.spatial.position.x + 0.1 &&
          pos.y <= this.spatial.position.y &&
          pos.y >= this.spatial.position.y + 0.1) {
        this.hasSponge = true;
      }
    }
    let inputs = this.getInputtableEntities();
    inputs.forEach(el => {
      /**
       * movement reduces health
       */
      if (el.state['UP']) {
        // move from 0.125 to 1
        this.spatial.position.y += this.spatial.position.y < 0.9 ? 0.01 : 0;
      } else if (el.state['DOWN']) {
        // inverse
        this.spatial.position.y -= this.spatial.position.y > 0.125 ? 0.01 : 0;
      }

      if (el.state['RIGHT']) {
        this.spatial.position.x += this.spatial.position.x < 0.9 ? 0.01 : 0;
        this.facingRight = true;
      } else if (el.state['LEFT']) {
        this.spatial.position.x -= this.spatial.position.x > 0 ? 0.01 : 0;
        this.facingRight = false;
      }

      if (el.state['A']) {
        // forrage
        // on forrage drop sponge
        // forrage reduces health if there is no sponge attached
        this.forrageTimer = time;
        this.isForraging = true;
        this.spatial.position.y = 0.125;

        // if doesn't have sponge, reduce health,
        // if does, drop sponge and reset position
        if (this.hasSponge) {
          this.hasSponge = false;
          this.delegate.droppedSponge();
        } else {
          this.delegate.reduceHealth();
        }

        let col = this.spatial.position.x;
        col += this.facingRight ? 0.1 : 0;
        col = Math.min(col, 1);
        col = Math.max(0, col);

        // Forrage area?
        let area = Math.floor(col / 0.0625);

        this.delegate.forragePoint(area);
      }
    });

    this.previousTime = time;
  }

  draw() {
    let gl = this.delegate.glContext();

    gl.useProgram(this.program);
    gl.bindVertexArray(this.vao);
    let offsetUniform = gl.getUniformLocation(this.program, 'offset');
    gl.uniform2f(
        offsetUniform, this.spatial.position.x, this.spatial.position.y);
    let directionUniform = gl.getUniformLocation(this.program, 'direction');
    gl.uniform1f(directionUniform, this.facingRight ? 1.0 : 0.0);

    let forragingUniform = gl.getUniformLocation(this.program, 'forraging');
    gl.uniform1f(forragingUniform, this.isForraging ? 1.0 : 0.0);

    gl.drawArrays(gl.TRIANGLES, 0, 6);
  }

  /**
   * Spatial component delegate methods
   */

  offsetModelMatrix() {
    /**
     * I guess if this had a parent, we could check for that and get it's world
     * matrix, or alternatively just return an identity
     */
    return this.delegate.spatial.worldModelMatrix();
  }

  /**
   * Position/Camera delegate methods
   */

  getInputtableEntities() {
    return this.delegate.getInputtableEntities();
  }
}
