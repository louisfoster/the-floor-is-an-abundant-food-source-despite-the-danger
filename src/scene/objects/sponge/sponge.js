import {SpatialComponent} from '../../../components/spatialComponent';
import {setupShaders} from '../../shaders/shaders';

export default class Sponge {
  constructor(_delegate) {
    this.delegate = _delegate;

    this.isSpatial = true;
    this.spatial = new SpatialComponent(this);
    this.offsetModelMatrix = this.offsetModelMatrix.bind(this);

    this.isVisible = true;
    this.draw = this.draw.bind(this);

    this.isUpdatable = true;
    this.update = this.update.bind(this);

    let gl = this.delegate.glContext();
    this.program;
    this.vao;
    this.vbo;
    this.setShaderData = this.setShaderData.bind(this);
    let meshArray = {
      // Top
      // top left
      'top-top-left': [0, 1, 1, 1, 0],
      'top-bottom-left': [
        // bottom left
        0,
        0,
        1,
        1,
        0,
      ],
      'top-top-right': [
        // top right
        1,
        1,
        1,
        1,
        0,
      ],
      'top-bottom-right': [
        // bottom right
        1,
        0,
        1,
        1,
        0,
      ],
    };
    let arr = [];
    arr.push(
        ...meshArray['top-top-right'], ...meshArray['top-top-left'],
        ...meshArray['top-bottom-left'],

        ...meshArray['top-bottom-left'], ...meshArray['top-bottom-right'],
        ...meshArray['top-top-right']);

    this.mesh = new Float32Array(arr);
    setupShaders(gl, 'other', 'other', this, () => {
      this.setShaderData(gl);
    });
  }

  setShaderData(gl) {
    this.vao = gl.createVertexArray();
    gl.bindVertexArray(this.vao);

    this.vbo = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, this.vbo);
    gl.bufferData(gl.ARRAY_BUFFER, this.mesh, gl.STATIC_DRAW);

    const vertexAttributeLocation =
        gl.getAttribLocation(this.program, 'a_vertex');
    gl.enableVertexAttribArray(vertexAttributeLocation);
    gl.vertexAttribPointer(
        vertexAttributeLocation, 2, gl.FLOAT, false, 5 * 4, 0);

    const colorAttributeLocation =
        gl.getAttribLocation(this.program, 'a_color');
    gl.enableVertexAttribArray(colorAttributeLocation);
    gl.vertexAttribPointer(
        colorAttributeLocation, 3, gl.FLOAT, false, 5 * 4, 2 * 4);

    this.delegate.objectReady();
  }

  update(time) {
    let v = this.delegate.spongePosition();
    if (v) {
      // sponge will either be located at random data point
      // or it will be located at dolphin point
      this.spatial.position.x = v[0];
      this.spatial.position.y = v[1];
    }
  }

  draw() {
    let gl = this.delegate.glContext();

    gl.useProgram(this.program);
    gl.bindVertexArray(this.vao);
    let offsetUniform = gl.getUniformLocation(this.program, 'offset');
    gl.uniform2f(
        offsetUniform, this.spatial.position.x, this.spatial.position.y);

    gl.drawArrays(gl.TRIANGLES, 0, 6);
  }

  offsetModelMatrix() {
    /**
     * I guess if this had a parent, we could check for that and get it's world
     * matrix, or alternatively just return an identity
     */
    return this.delegate.spatial.worldModelMatrix();
  }
}
