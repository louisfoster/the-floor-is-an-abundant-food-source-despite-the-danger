import {setupShaders} from '../../shaders/shaders';

export default class World {
  constructor(_delegate) {
    this.delegate = _delegate;

    this.isVisible = true;
    this.draw = this.draw.bind(this);

    let gl = this.delegate.glContext();
    this.program;
    this.vao;
    this.vbo;
    this.setShaderData = this.setShaderData.bind(this);
    let floorHeight = 1 / 8;
    let meshArray = {
      // Top
      // top left
      'top-top-left': [0, 1, 0, 1, 1],
      'top-bottom-left': [
        // bottom left
        0,
        floorHeight,
        0,
        1,
        1,
      ],
      'top-top-right': [
        // top right
        1,
        1,
        0,
        1,
        1,
      ],
      'top-bottom-right': [
        // bottom right
        1,
        floorHeight,
        0,
        1,
        1,
      ],
      // Floor
      'floor-top-left': [0, floorHeight, 0, 1, 0],
      'floor-bottom-left': [0, 0, 0, 1, 0],
      'floor-top-right': [1, floorHeight, 0, 1, 0],

      'floor-bottom-right': [1, 0, 0, 1, 0],
    };
    let arr = [];
    arr.push(
        ...meshArray['top-top-right'], ...meshArray['top-top-left'],
        ...meshArray['top-bottom-left'],

        ...meshArray['top-bottom-left'], ...meshArray['top-bottom-right'],
        ...meshArray['top-top-right'],

        ...meshArray['floor-top-left'], ...meshArray['floor-bottom-left'],
        ...meshArray['floor-top-right'],

        ...meshArray['floor-bottom-left'], ...meshArray['floor-bottom-right'],
        ...meshArray['floor-top-right']);

    this.mesh = new Float32Array(arr);
    setupShaders(gl, 'scene', 'scene', this, () => {
      this.setShaderData(gl);
    });
  }

  setShaderData(gl) {
    this.vao = gl.createVertexArray();
    gl.bindVertexArray(this.vao);

    this.vbo = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, this.vbo);
    gl.bufferData(gl.ARRAY_BUFFER, this.mesh, gl.STATIC_DRAW);

    const vertexAttributeLocation =
        gl.getAttribLocation(this.program, 'a_vertex');
    gl.enableVertexAttribArray(vertexAttributeLocation);
    gl.vertexAttribPointer(
        vertexAttributeLocation, 2, gl.FLOAT, false, 5 * 4, 0);

    const colorAttributeLocation =
        gl.getAttribLocation(this.program, 'a_color');
    gl.enableVertexAttribArray(colorAttributeLocation);
    gl.vertexAttribPointer(
        colorAttributeLocation, 3, gl.FLOAT, false, 5 * 4, 2 * 4);

    this.delegate.objectReady();
  }

  draw() {
    let gl = this.delegate.glContext();

    gl.useProgram(this.program);
    gl.bindVertexArray(this.vao);
    gl.drawArrays(gl.TRIANGLES, 0, 12);
  }
}
