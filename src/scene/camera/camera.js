import { mat4 as m4, vec3 } from 'gl-matrix';

import { SpatialComponent } from '../../components/spatialComponent';

export default class Camera {
    constructor(
        _delegate,
        _viewWidth,
        _viewHeight,
        _fieldOfView = 85.0,
        _nearPlane = 0.01,
        _farPlane = 100.0
    ) {
        /**
         * Delegate provides:
         *   - getInputtableEntities() to retrieve input state from parent
         *   - spatial.worldModelMatrix() component method
         */
        this.delegate = _delegate;

        this.isCamera = true;
        this.isSpatial = true;
        this.spatial = new SpatialComponent(this);
        this.offsetModelMatrix = this.offsetModelMatrix.bind(this);

        this.viewWidth = _viewWidth;
        this.viewHeight = _viewHeight;
        this.fieldOfView = _fieldOfView;
        this.nearPlane = _nearPlane;
        this.farPlane = _farPlane;

        this.buildProjectionMatrix = this.buildProjectionMatrix.bind(this);
        this.projection = this.buildProjectionMatrix();

        this.resize = this.resize.bind(this);

        this.view = () => {
            let matrix = m4.create();
            m4.invert(matrix, this.spatial.worldModelMatrix());
            return matrix;
        };
    }

    resize(newWidth, newHeight) {
        this.viewWidth = newWidth;
        this.viewHeight = newHeight;
        this.projection = this.buildProjectionMatrix();
    }

    update() {
        let inputs = this.delegate.getInputtableEntities();
        inputs.forEach(el => {
            /**
             * Rotation joystick along x axis (left-right) ie yaw
             */
            let xAxisRotate = el.state['RX'];
            if (xAxisRotate !== 0 && Math.abs(xAxisRotate) > 0.1) {
                this.spatial.rotation.y += xAxisRotate;
            }
            /**
             * Rotation joystick along y axis (up-down) ie pitch
             */
            let yAxisRotate = el.state['RY'];
            if (yAxisRotate !== 0 && Math.abs(yAxisRotate) > 0.1) {
                this.spatial.rotation.x += yAxisRotate;
            }
        });
    }

    buildProjectionMatrix() {
        let aspectRatio = this.viewWidth / this.viewHeight;
        let matrix = m4.create();
        m4.perspective(
            matrix,
            this.fieldOfView,
            aspectRatio,
            this.nearPlane,
            this.farPlane
        );
        return matrix;
    }

    /**
     * Spatial delegate methods
     */

    offsetModelMatrix() {
        return this.delegate.spatial.worldModelMatrix();
    }
}
