import { mat4 as m4 } from 'gl-matrix';

import DebugComponent from '../components/debugComponent';
import { SpatialComponent } from '../components/spatialComponent';

import Player from './objects/player/player';
import Sponge from './objects/sponge/sponge';
import World from './objects/world/world';

export default class Scene {
    constructor(_delegate, viewWidth, viewHeight) {
        this.delegate = _delegate;

        this.isSpatial = true;
        this.spatial = new SpatialComponent(this);
        this.offsetModelMatrix = this.offsetModelMatrix.bind(this);

        this.player = new Player(this);
        this.getInputtableEntities = this.getInputtableEntities.bind(this);
        this.forragePoint = this.forragePoint.bind(this);
        this.reduceHealth = this.reduceHealth.bind(this);
        this.droppedSponge = this.droppedSponge.bind(this);

        this.world = new World(this);
        this.objectReady = this.objectReady.bind(this);

        this.sponge = new Sponge(this);
        this.spongePosition = this.spongePosition.bind(this);

        this.isUpdatable = true;
        this.update = this.update.bind(this);

        this.resize = this.resize.bind(this);

        this.isDebuggable = true;
        this.debug = new DebugComponent('Scene');

        this.draw = this.draw.bind(this);
        this.children = [this.world, this.player, this.sponge];

        this.drawableCount = 0;
        this.children.forEach(child => {
            if (child.isVisible) this.drawableCount += 1;
        });
    }

    resize(newWidth, newHeight) {
        // For each child with a camera, resize camera?
        // this.children.forEach(child => {
        //   if (child.camera) child.camera.resize(newWidth, newHeight);
        // });
    }

    update(time) {
        // Update children, let children update their cameras
        this.children.forEach(child => {
            if (child.isUpdatable) child.update(time);
        });
    }

    draw() {
        let gl = this.delegate.glContext();
        gl.clear(gl.COLOR_BUFFER_BIT);
        gl.enable(gl.DEPTH_TEST);

        this.children.forEach(child => {
            if (child.isVisible) child.draw(gl);
        });
    }

    /**
     * Player delegate methods
     */
    getInputtableEntities() {
        return this.delegate.getInputtableEntities();
    }

    /**
     * Spatial delegate methods
     */
    offsetModelMatrix() {
        return m4.create();
    }

    /**
     * Object delegate methods
     */

    glContext() {
        return this.delegate.glContext();
    }

    objectReady() {
        this.drawableCount -= 1;
        if (this.drawableCount === 0) this.delegate.sceneReady();
    }

    /**
     * Player delegate methods
     */

    forragePoint(area) {
        this.delegate.handleForrageEvent(area);
    }
    droppedSponge() {
        this.delegate.handleDroppedSponge();
    }
    reduceHealth() {
        this.delegate.handleReduceHealth();
    }

    /**
     * Sponge delegate methods
     */
    spongePosition() {
        return this.player.hasSponge
            ? [
                  this.player.spatial.position.x + 0.1,
                  this.player.spatial.position.y + 0.1,
              ]
            : this.delegate.getSpongePosition();
    }
}
