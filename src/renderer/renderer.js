export default class Renderer {
  constructor(_delegate) {
    let canvas = document.getElementById('render');
    this.gl = canvas.getContext('webgl2');
    this.delegate = _delegate;
    this.resizeCanvas = this.resizeCanvas.bind(this);
    this.setup = this.setup.bind(this);
    this.setup();

    this.resize = this.resize.bind(this);
    window.addEventListener('resize', this.resize, false);
  }

  setup() {
    this.resizeCanvas(this.gl.canvas);
    this.gl.viewport(0, 0, this.gl.canvas.width, this.gl.canvas.height);
    this.gl.clearColor(1.0, 1.0, 1.0, 1.0);
  }

  resize() {
    this.setup();
    this.delegate.resizeEvent(this.gl.canvas.width, this.gl.canvas.height);
  }

  resizeCanvas() {
    var cssToRealPixels = window.devicePixelRatio || 1;

    // Lookup the size the browser is displaying the canvas in CSS pixels
    // and compute a size needed to make our drawingbuffer match it in
    // device pixels.
    var displayWidth = Math.floor(this.gl.canvas.clientWidth * cssToRealPixels);
    var displayHeight =
        Math.floor(this.gl.canvas.clientHeight * cssToRealPixels);

    // Check if the canvas is not the same size.
    if (this.gl.canvas.width !== displayWidth ||
        this.gl.canvas.height !== displayHeight) {
      // Make the canvas the same size
      this.gl.canvas.width = displayWidth;
      this.gl.canvas.height = displayHeight;
    }
  }
}
