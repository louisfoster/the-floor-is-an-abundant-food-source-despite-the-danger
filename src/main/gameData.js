import DebugComponent from '../components/debugComponent';

export default class GameData {
  constructor() {
    this.fishLocation = 5;
    this.health = 1.0;
    this.spongeLocation = [0.5, 0.5];
    this.previousTime = 0;

    this.isDebuggable = true;
    this.debug = new DebugComponent('Data');

    this.isUpdatable = true;
    this.update = this.update.bind(this);

    this.reduceHealth = this.reduceHealth.bind(this);
    this.updateFishLocation = this.updateFishLocation.bind(this);
    this.updateSpongeLocation = this.updateSpongeLocation.bind(this);
  }

  update(time) {
    /**
     * reduce health over time
     */
    this.debug.clearText();
    this.debug.addLine(`Health: ${Math.round(this.health * 100)}`);

    let delta = time - this.previousTime;
    this.health -= delta * 0.000001;
  }

  reduceHealth(amount) {
    /**
     * reduce health by x amount
     */
    this.health -= 0.1;
  }

  updateFishLocation(area) {
    /**
     * Area 0-15
     */
    if (area > this.fishLocation) {
      // move left
      this.fishLocation = Math.max(0, this.fishLocation - 3);
    } else {
      // move right
      this.fishLocation = Math.min(15, this.fishLocation + 3);
    }
  }

  updateSpongeLocation(x, y) {
    /**
     * x,y offset
     */
    this.spongeLocation[0] = Math.random();
    this.spongeLocation[1] = Math.max(0.125, Math.random());
  }
}
