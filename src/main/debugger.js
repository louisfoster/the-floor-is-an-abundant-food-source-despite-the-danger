export default class Debugger {
    constructor(_delegate) {
        this.delegate = _delegate;
        this.debugOut = document.getElementById('debug');
        this.log = this.log.bind(this);
        this.hidden = false;
        this.isUpdatable = true;
        this.inputCheck = false;
        this.debounce = false;
        this.update = this.update.bind(this);
    }
    update() {
        let inputs = this.delegate.getInputtableEntities();
        inputs.forEach(el => {
            if (el.state['D'] && !this.debounce) {
                // if true, and not previous input check true, toggle hidden and hide/show block
                // if true and previous input check true, ignore
                this.debounce = true;
                this.inputCheck = true;
                setTimeout(() => {
                    this.debounce = false;
                }, 200);
            } else {
                // set previous input check to false
                // this.inputCheck = false;
                if (this.inputCheck) {
                    this.debounce = true;
                    this.inputCheck = false;
                    this.hidden = !this.hidden;
                    this.debugOut.style = this.hidden ? 'display: none' : '';
                }
            }
        });
        if (!this.hidden) this.log();
    }

    log() {
        let text = '';
        // Return array of all entities with "isDebuggable == true"
        let debugEntities = this.delegate.getDebuggableEntities();
        debugEntities.forEach(element => {
            text += `${element.debug.getText()}`;
        });
        this.debugOut.textContent = text;
    }
}
