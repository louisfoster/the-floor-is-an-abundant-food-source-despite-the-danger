// Adapted from the threejs clock from alteredq
import DebugComponent from '../components/debugComponent';

export default class Clock {
    constructor() {
        this.startTime = 0;
        this.oldTime = 0;
        this.elapsedTime = 0;
        this.running = false;
        this.currentSecond = 0;
        this.framecounter = 0;
        this.currentFramerate = 0;
        this.isDebuggable = true;
        this.debug = new DebugComponent('Time');
        this.isUpdatable = true;

        this.start = this.start.bind(this);
        this.stop = this.stop.bind(this);
        this.getElapsedTime = this.getElapsedTime.bind(this);
        this.getDelta = this.getDelta.bind(this);
        this.framerate = this.framerate.bind(this);
        this.update = this.update.bind(this);
    }

    start() {
        this.startTime = performance.now();
        this.oldTime = this.startTime;
        this.elapsedTime = 0;
        this.currentSecond = 0;
        this.framecounter = 1;
        this.currentFramerate = 1;
        this.running = true;
    }

    stop() {
        this.getElapsedTime();
        this.running = false;
    }

    update() {
        this.debug.clearText();

        let e = this.getElapsedTime();
        this.debug.addLine(`Elapsed: ${e}`);

        let f = this.framerate();
        this.debug.addLine(`FPS: ${f}`);
    }

    getElapsedTime() {
        this.getDelta();
        return this.elapsedTime;
    }

    getDelta() {
        let diff = 0;
        if (this.running === false) {
            this.start();
            return 0;
        } else {
            const newTime = performance.now();
            diff = (newTime - this.oldTime) / 1000;
            this.oldTime = newTime;
            this.elapsedTime += diff;
        }

        return diff;
    }

    framerate() {
        const elapsed = ~~~this.getElapsedTime();
        if (elapsed === this.currentSecond) {
            this.framecounter += 1;
            return this.currentFramerate;
        } else {
            this.currentFramerate = this.framecounter;
            this.framecounter = 1;
            this.currentSecond = elapsed;
            return this.currentFramerate;
        }
    }
}
