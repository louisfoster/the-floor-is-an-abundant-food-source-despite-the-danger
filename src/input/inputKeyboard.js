import DebugComponent from '../components/debugComponent';

export default class InputKeyboard {
    constructor() {
        // Input types have "state" objects
        this.isInputtable = true;
        this.state = {};

        this.isDebuggable = true;
        this.debug = new DebugComponent('Input Keyboard');
        this.isUpdatable = true;
        this.validKeys = ['D', 'A'];
        this.validKeys.forEach(el => {
            this.state[el] = false;
        });

        this.update = this.update.bind(this);
        this.handleKeyUp = this.handleKeyUp.bind(this);
        this.handleKeyDown = this.handleKeyDown.bind(this);
        window.addEventListener('keydown', this.handleKeyDown, false);
        window.addEventListener('keyup', this.handleKeyUp, false);
    }
    update() {
        this.debug.clearText();
        this.debug.addLine('Keys:');
        this.validKeys.forEach(el => {
            this.debug.addLine(`${el}: pressed(${this.state[el]})`);
        });
    }
    handleKeyDown(event) {
        event.preventDefault();
        event.stopPropagation();

        this.state[event.key] = true;
    }
    handleKeyUp(event) {
        event.preventDefault();
        event.stopPropagation();

        this.state[event.key] = false;
    }
}
