/*
    Debug looks for all entities with a "isDebuggable == true"
    And renders from their debug property (this class), getting
    output text with the getText() method.
*/

export default class DebugComponent {
    constructor(_name) {
        this.name = _name;
        this.lines = [];
        this.addLine = this.addLine.bind(this);
        this.clearText = this.clearText.bind(this);
        this.getText = this.getText.bind(this);
    }

    addLine(text) {
        this.lines.push(text);
    }

    clearText() {
        this.lines = [];
    }

    getText() {
        let text = '';
        if (this.lines.length > 0) {
            text += `${this.name}:\n`;
            this.lines.forEach(el => {
                text += `\n${el}\n`;
            });
            text += `\n--\n\n`;
        }
        return text;
    }
}
