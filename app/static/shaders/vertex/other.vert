attribute vec2 a_vertex;
attribute vec3 a_color;
uniform vec2 offset;
varying vec4 v_color;

void main() {
  v_color = vec4(a_color, 1);
  vec2 scale = (a_vertex * 0.05 + offset) * 2.0 - 1.0;
  gl_Position = vec4(scale, -1, 1);
}
