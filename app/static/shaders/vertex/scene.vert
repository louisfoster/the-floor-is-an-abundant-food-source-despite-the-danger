attribute vec2 a_vertex;
attribute vec3 a_color;
varying vec4 v_color;

void main() {
  v_color = vec4(a_color, 1);
  gl_Position = vec4(a_vertex * 2.0 - 1.0, 0, 1);
}
