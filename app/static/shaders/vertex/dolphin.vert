attribute vec2 a_vertex;
attribute vec3 a_color;
uniform vec2 offset;
uniform float direction;
uniform float forraging;
varying vec4 v_color;

void main() {
  v_color = vec4(a_color, 1);
  if (a_vertex.x == direction) {
    v_color = vec4(1, 1, 1, 1);
  }
  if (forraging == 1.0) {
    v_color = vec4(0, 0, 1, 1);
  }
  vec2 scale = (a_vertex * 0.1 + offset) * 2.0 - 1.0;
  gl_Position = vec4(scale, -1, 1);
}
